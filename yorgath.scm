(define-module (yorgath)
  ;;
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages texinfo)
  ;;
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system guile)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  ;;
  #:use-module ((guix licenses) #:prefix license:))

(define-public guile-yamlpp
  (package
    (name "guile-yamlpp")
    (version "0.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/yorgath/guile-yamlpp")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ik69y0vddg0myp0zdbkmklma0qkkrqzwlqwkij1zirklz6hl1ss"))))
    (build-system gnu-build-system)
    (native-inputs (list autoconf
                         automake
                         libtool
                         pkg-config))
    (inputs (list guile-3.0
                  yaml-cpp))
    (native-search-paths (list (search-path-specification
                                (variable "GUILE_EXTENSIONS_PATH")
                                (files (list "lib/guile/3.0")))))
    (home-page "https://gitlab.com/yorgath/guile-yamlpp")
    (synopsis "Guile YAML reader/writer based on @code{yaml-cpp}")
    (description
     "A module for GNU Guile to read and write YAML files.  It works using
bindings to the @code{yaml-cpp} C++ library.")
    (license license:gpl3+)))
